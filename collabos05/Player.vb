﻿Public Class Player
    Public name As String
    Public points As Integer
    Public remaining As Integer = 10
    Public throws As Integer = 0
    Public result As String = ""



    Public Function rnd(min, max) As Integer
        Static rn As New Random
        Return rn.Next(min, max)
    End Function

    Public Sub newround()
        throws = 0
        remaining = 10
    End Sub

    Public Sub New(usr As String)
        name = usr
        points = 0
        remaining = 10
        throws = 0
    End Sub

    Public Function returnPoints() As String
        Return name & ":" & points
    End Function

    Public Sub getresult()
        If throws = 2 And remaining > 0 Then result = name & " " & remaining
        If remaining = 0 And throws = 1 Then result = name & " " & "Strike"
        If remaining = 0 And throws = 2 Then result = name & " " & "Spare"
    End Sub

    Public Sub move()
        Threading.Thread.Sleep(10)
        If throws >= 2 Then Exit Sub
        If remaining = 0 Then Exit Sub
        throws += 1
        Dim balls As Integer = rnd(0, remaining + 1)
        points += balls
        remaining -= balls
    End Sub


End Class
