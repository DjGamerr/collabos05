﻿Public Class Form1

    Public Function rnd(min, max) As Integer
        Static rn As New Random
        Return rn.Next(min, max)
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ListBox1.Items.Clear()
        Dim players As New List(Of Player)
        players.Add(New Player("Player 1"))
        players.Add(New Player("Player 2"))

        For i As Integer = 1 To 10 Step 1
            ListBox1.Items.Add("-----------------")
            ListBox1.Items.Add("Frame:" & i)
            ListBox1.Items.Add("-----------------")
            For Each p As Player In players
                p.newround()
                While p.throws < 2 And p.remaining > 0
                    p.move()
                End While
                p.getresult()
                ListBox1.Items.Add(p.result)
            Next
        Next
        ListBox1.Items.Add("-----------------")
        ListBox1.Items.Add("Total points:")
        For Each p As Player In players
            ListBox1.Items.Add(p.returnPoints)
        Next
    End Sub
End Class
